package com.test.a20220516_abdulhameedbasha_nycschools

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ListView
import android.widget.TextView
import com.test.a20220516_abdulhameedbasha_nycschools.model.SchoolDetails
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DetailsActivity : AppCompatActivity(), SchoolDetailsContractor.View {
    private lateinit var txtSchoolName: TextView
    private lateinit var txtMathScore: TextView
    private lateinit var txtReadingScore: TextView
    private lateinit var txtWritingScore: TextView
    private val presenter= SchoolDetailsPresenter(this)
    private var index = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        txtSchoolName = findViewById<TextView>(R.id.schoolName)
        txtMathScore = findViewById<TextView>(R.id.mathScore)
        txtReadingScore = findViewById<TextView>(R.id.readingScore)
        txtWritingScore = findViewById<TextView>(R.id.writingScore)

         index = intent.getIntExtra("selected",0)
        presenter.GetSchoolDetails()
    }

    override fun onSuccess(schoolDetails: List<SchoolDetails>) {
        runOnUiThread{
            txtMathScore.text = getString(R.string.sat_score_writing)+schoolDetails.get(index).sat_math_avg_score
            txtSchoolName.text = getString(R.string.school_name)+schoolDetails.get(index).school_name
            txtWritingScore.text = getString(R.string.sat_score_writing)+schoolDetails.get(index).sat_writing_avg_score
            txtReadingScore.text = getString(R.string.sat_score_reading)+schoolDetails.get(index).sat_critical_reading_avg_score
        }
    }
}