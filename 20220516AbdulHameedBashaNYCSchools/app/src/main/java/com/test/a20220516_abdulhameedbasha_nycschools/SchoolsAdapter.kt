package com.test.a20220516_abdulhameedbasha_nycschools

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.TextView
import com.test.a20220516_abdulhameedbasha_nycschools.model.School
import com.test.a20220516_abdulhameedbasha_nycschools.model.SchoolDetails

class SchoolsAdapter(val context: Context,
                     val schools: List<School>) : BaseAdapter() {

    override fun getCount(): Int {

        if(schools != null && schools.size > 0){
            return schools.size
        }
        return 0
    }

    override fun getItem(p0: Int): Any {
        TODO("Not yet implemented")
    }

    override fun getItemId(p0: Int): Long {
        return 0
    }

    override fun getView(position: Int, p1: View?, p2: ViewGroup?): View {

        var inflater: LayoutInflater = LayoutInflater.from(context)
        var myView = inflater.inflate(R.layout.row_schools,null,false)

        var textSchool = myView.findViewById<TextView>(R.id.schoolName)
        var textEmail = myView.findViewById<TextView>(R.id.schoolEmail)
        var btnShowDetails = myView.findViewById<Button>(R.id.showDetails)

        textSchool.text = schools.get(position).school_name
        textEmail.text = schools.get(position).school_email

        btnShowDetails.setOnClickListener{
            var intent = Intent(context.applicationContext, DetailsActivity::class.java)
            intent.apply {
                putExtra("selected",position)
            }
            intent.setFlags(FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }

        return  myView
    }
}