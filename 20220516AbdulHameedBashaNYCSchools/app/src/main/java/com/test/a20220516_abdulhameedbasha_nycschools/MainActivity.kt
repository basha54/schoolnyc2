package com.test.a20220516_abdulhameedbasha_nycschools

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ListView
import com.test.a20220516_abdulhameedbasha_nycschools.model.School
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity(), SchoolContractor.View {
    private lateinit var listView: ListView
    private lateinit var adapter: SchoolsAdapter
    private val presenter = SchoolPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listView = findViewById(R.id.schoolsList)

        //Presenter gets schools list
        presenter.GetSchoolsList()
    }

    override fun onListSuccess(listSchool: List<School>) {
        runOnUiThread{
            adapter = SchoolsAdapter(applicationContext, listSchool.toList())

            listView.adapter =adapter
            Log.i("Response",listSchool.size.toString())
        }
    }
}