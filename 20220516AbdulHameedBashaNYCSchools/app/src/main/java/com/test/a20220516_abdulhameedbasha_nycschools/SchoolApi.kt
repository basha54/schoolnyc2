package com.test.a20220516_abdulhameedbasha_nycschools

import com.test.a20220516_abdulhameedbasha_nycschools.model.School
import com.test.a20220516_abdulhameedbasha_nycschools.model.SchoolDetails
import retrofit2.http.GET

interface SchoolApi {

    @GET("s3k6-pzi2.json")
    suspend fun  getSchools() : List<School>

    @GET("f9bf-2cp4.json")
    suspend fun  getSchoolDetails() : List<SchoolDetails>
}