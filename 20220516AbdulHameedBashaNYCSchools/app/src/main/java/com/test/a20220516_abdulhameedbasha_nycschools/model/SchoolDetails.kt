package com.test.a20220516_abdulhameedbasha_nycschools.model

data class SchoolDetails(
    val dbn: String,
    val school_name:String,
    val sat_math_avg_score:String,
    val sat_writing_avg_score: String,
    val sat_critical_reading_avg_score: String
)
