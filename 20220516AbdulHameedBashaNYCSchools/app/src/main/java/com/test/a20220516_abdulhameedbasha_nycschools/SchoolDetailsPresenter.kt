package com.test.a20220516_abdulhameedbasha_nycschools

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SchoolDetailsPresenter(val view: SchoolDetailsContractor.View): SchoolDetailsContractor.Presenter {

    override fun GetSchoolDetails() {
        val api = RetrofitHelper.getInstance().create(SchoolApi::class.java)

        GlobalScope.launch {
            val result = api.getSchoolDetails()

            if(result != null){
                view.onSuccess(result)
            }
        }
    }
}