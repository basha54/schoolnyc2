package com.test.a20220516_abdulhameedbasha_nycschools

import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class SchoolPresenter (
    val view: SchoolContractor.View
    ): SchoolContractor.Presenter {
    override fun GetSchoolsList() {
        val api = RetrofitHelper.getInstance().create(SchoolApi::class.java)

        GlobalScope.launch {
            val result = api.getSchools()

            if(result != null){
              view.onListSuccess(result)
            }
        }
    }
}