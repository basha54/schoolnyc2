package com.test.a20220516_abdulhameedbasha_nycschools

import com.test.a20220516_abdulhameedbasha_nycschools.model.School

interface SchoolContractor {
    interface View{
        fun onListSuccess(listSchool: List<School>)
    }
    interface Presenter{
        fun GetSchoolsList()
    }

}