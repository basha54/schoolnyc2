package com.test.a20220516_abdulhameedbasha_nycschools

import com.test.a20220516_abdulhameedbasha_nycschools.model.SchoolDetails

interface SchoolDetailsContractor {

    interface View{
        fun onSuccess(schoolDetails:List<SchoolDetails>)
    }

    interface Presenter{
        fun GetSchoolDetails()
    }
}