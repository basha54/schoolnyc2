package com.test.a20220516_abdulhameedbasha_nycschools.model

data class School(
    val dbn: String,
    val school_name: String,
    val school_email: String
)
